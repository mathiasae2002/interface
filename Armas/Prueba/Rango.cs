﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Rango : Enemigo, IDanoOperation, IEstadoOperation
    {
        private float vida;
        private float dano;
        public Rango(string nombre,float vida, float dano) : base("Rango",nombre)
        {
            this.vida = vida;
            this.dano = dano;
        }
        public float CalcularDano()
        {
            return vida - dano;
        }
        public float EstadoEnemy()
        {

            return vida;
        }
    }
}
