﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    abstract class Enemigo
    {
        protected string nombre;
        protected string tipo;
        protected Enemigo(string tipo,string nombre)
        {
            this.nombre = nombre;
            this.tipo = tipo;
        }
        public string GetDetalles()
        {
            return $"El enemigo es de tipo {tipo} y su categoria es {nombre}";
        }
    }
}
