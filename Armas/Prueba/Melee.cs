﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Melee : Enemigo, IDanoOperation, IEstadoOperation
    {
        private float vida;
        private float dano;
        public Melee(string nombre,float vida, float dano):base("Melee",nombre)
        {
            this.vida = vida;
            this.dano = dano;
        }
        public float CalcularDano()
        {
            return vida - dano;
        }
        public float EstadoEnemy()
        {

            return vida;
        }
    }
}
