﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Triangulo : Figura, IFiguraAreaOperacion
    {
        private float b;
        private float altura;
        public Triangulo(string nombre,float b, float altura) : base("Triangulo",nombre) 
        {
            this.b = b;
            this.altura = altura;
        }
        public float CalcularArea()
        {
            return b * altura / 2;
        }
    }
}
