﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    abstract class Figura
    {
        protected string nombre;
        protected string tipo;
        protected Figura(string tipo,string nombre)
        {
            this.nombre = nombre;
            this.tipo = tipo;
        }

        public string GetDetalles()
        {
            return $"La figura de tipo {tipo} tiene el nombre de {nombre}";
        }
    }
}
