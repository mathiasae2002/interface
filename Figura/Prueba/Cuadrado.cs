﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Cuadrado : Figura, IFiguraAreaOperacion
    {
        private float lado;
        public Cuadrado(string nombre,float lado):base("Cuadrado",nombre)
        {
            this.lado = lado;
        }
        public float CalcularArea()
        {
            return lado*lado;
        }
    }
}
