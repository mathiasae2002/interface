﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Program
    {
        static void Main(string[] args)
        {
            string a;
            int r;
            int p;
            string b;
            string c="Operar";
            List<IFiguraAreaOperacion> figuraareas = new List<IFiguraAreaOperacion>();
            while (c == "Operar") { 
            Console.WriteLine("Seleccione una figura:(1=Circulo,2=Triangulo,3=Cuadrado,4=Rectangulo y 5=Salir)");
            c = Console.ReadLine();
            if (c == "1")
            {
                Console.WriteLine("Radio:" );
                a = Console.ReadLine();
                r = Int16.Parse(a);
                figuraareas.Add(new Circulo("Circulo 1", r));
                foreach(var figuraarea in figuraareas)
                    {
                        Console.WriteLine(figuraarea.CalcularArea());
                    }
                figuraareas.Clear();
                Console.WriteLine("¿Intentar de nuevo? (Si/No)");
                c=Console.ReadLine();
                    if (c == "Si")
                    {
                        c = "Operar";
                    }
                    else if (c == "No")
                    {
                        c = "4";
                    }
            }
            else if (c == "2")
            {
                 Console.WriteLine("Base:");
                 a = Console.ReadLine();
                    Console.WriteLine("Altura:");
                    b = Console.ReadLine();
                    r = Int16.Parse(a);
                    p = Int16.Parse(b);
                    figuraareas.Add(new Triangulo("Triangulo 1", r,p));
                    foreach (var figuraarea in figuraareas)
                    {
                        Console.WriteLine(figuraarea.CalcularArea());
                    }
                    figuraareas.Clear();
                    Console.WriteLine("¿Intentar de nuevo? (Si/No)");
                    c = Console.ReadLine();
                    if (c == "Si")
                    {
                        c = "Operar";
                    }
                    else if (c == "No")
                    {
                        c = "4";
                    }

                }
            else if (c == "3")
            {
                    Console.WriteLine("Lado:");
                    a = Console.ReadLine();
                    r = Int16.Parse(a);
                    figuraareas.Add(new Cuadrado("Circulo 1", r));
                    foreach (var figuraarea in figuraareas)
                    {
                        Console.WriteLine(figuraarea.CalcularArea());
                    }
                    figuraareas.Clear();
                    Console.WriteLine("¿Intentar de nuevo? (Si/No)");
                    c = Console.ReadLine();
                    if (c == "Si")
                    {
                        c = "Operar";
                    }
                    else if (c == "No")
                    {
                        c = "4";
                    }
                }
            else if (c == "4")
            {
                    Console.WriteLine("Base:");
                    a = Console.ReadLine();
                    Console.WriteLine("Altura:");
                    b = Console.ReadLine();
                    r = Int16.Parse(a);
                    p = Int16.Parse(b);
                    figuraareas.Add(new Rectangulo("Circulo 1", r,p));
                    foreach (var figuraarea in figuraareas)
                    {
                        Console.WriteLine(figuraarea.CalcularArea());
                    }
                    figuraareas.Clear();
                    Console.WriteLine("¿Intentar de nuevo? (Si/No)");
                    c = Console.ReadLine();
                    if (c == "Si")
                    {
                        c = "Operar";
                    }
                    else if (c == "No")
                    {
                        c = "5";
                    }

                }
            else if(c=="5")
            {
                Console.WriteLine("Fin de la operacion");
            }
            }
            Console.ReadLine();
        } 
    } 
}