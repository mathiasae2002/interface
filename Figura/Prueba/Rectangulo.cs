﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Rectangulo : Figura, IFiguraAreaOperacion
    {
        private float b;
        private float altura;
        public Rectangulo(string nombre,float b,float altura) : base("Rectangulo",nombre)
        {
            this.b=b;
            this.altura = altura;
        }
        public float CalcularArea()
        {
            return b * altura;
        }
    }
}
