﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba
{
    class Circulo : Figura, IFiguraAreaOperacion
    {
        private float radio;
        public Circulo(string nombre,float radio) : base("Circulo",nombre)
        {
            this.radio = radio;
        }
        public float CalcularArea()
        {
            return radio*radio*3.14f;
        }
    }
}
